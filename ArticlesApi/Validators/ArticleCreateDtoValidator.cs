﻿using ArticlesApi.DTOs;
using FluentValidation;

namespace ArticlesApi.Validators
{
    public class ArticleCreateDtoValidator: AbstractValidator<ArticleCreateDto>
    {
        public ArticleCreateDtoValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required.")
                .MaximumLength(100).WithMessage("Title must not exceed 100 characters.");

            RuleFor(x => x.Content)
                .NotEmpty().WithMessage("Content is required.");

            RuleFor(x => x.PublishedDate)
                .LessThanOrEqualTo(DateTime.UtcNow).WithMessage("Published date cannot be in the future.");
        }
    }
}
