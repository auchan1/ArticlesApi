﻿namespace ArticlesApi.DTOs
{
    public class ArticleCreateDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime PublishedDate { get; set; }
    }
}
