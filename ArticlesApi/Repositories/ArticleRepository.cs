﻿using ArticlesApi.Data;
using ArticlesApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ArticlesApi.Repositories
{
    public class ArticleRepository: IArticleRepository
    {
        private readonly DataContext _context;

        public ArticleRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Article>> GetAllArticles()
        {
            return await _context.Articles.ToListAsync();
        }

        public async Task<Article> GetArticleById(int id)
        {
            return await _context.Articles.FindAsync(id);
        }

        public async Task CreateArticle(Article article)
        {
            await _context.Articles.AddAsync(article);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateArticle(Article article)
        {
            _context.Articles.Update(article);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteArticle(int id)
        {
            var article = await _context.Articles.FindAsync(id);
            if (article != null)
            {
                _context.Articles.Remove(article);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<(IEnumerable<Article>, int)> GetArticlesPaged(int page, int pageSize)
        {
            var totalArticles = await _context.Articles.CountAsync();
            var articles = await _context.Articles
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
            return (articles, totalArticles);
        }
    }
}
