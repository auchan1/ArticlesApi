﻿using ArticlesApi.Models;

namespace ArticlesApi.Repositories
{
    public interface IArticleRepository
    {
        Task<IEnumerable<Article>> GetAllArticles();
        Task<Article> GetArticleById(int id);
        Task CreateArticle(Article article);
        Task UpdateArticle(Article article);
        Task DeleteArticle(int id);
        Task<(IEnumerable<Article>, int)> GetArticlesPaged(int page, int pageSize);
    }
}
