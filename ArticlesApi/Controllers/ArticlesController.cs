﻿using ArticlesApi.DTOs;
using ArticlesApi.Models;
using ArticlesApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace ArticlesApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController: ControllerBase
    {
        private readonly IArticleRepository _repository;

        public ArticlesController(IArticleRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Article>>> GetAllArticles()
        {
            var articles = await _repository.GetAllArticles();
            return Ok(articles);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Article>> GetArticleById(int id)
        {
            var article = await _repository.GetArticleById(id);
            if (article == null)
            {
                return NotFound();
            }
            return Ok(article);
        }

        [HttpPost]
        [Authorize(Policy = "AdminOnly")]
        public async Task<ActionResult> CreateArticle([FromBody] ArticleCreateDto articleDto)
        {
            var article = new Article
            {
                Title = articleDto.Title,
                Content = articleDto.Content,
                PublishedDate = articleDto.PublishedDate
            };
            await _repository.CreateArticle(article);
            return CreatedAtAction(nameof(GetArticleById), new { id = article.Id }, article);
        }

        [HttpPut("{id}")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> UpdateArticle(int id, [FromBody] ArticleUpdateDto articleDto)
        {
            if (id != articleDto.Id)
            {
                return BadRequest();
            }

            var article = new Article
            {
                Id = articleDto.Id,
                Title = articleDto.Title,
                Content = articleDto.Content,
                PublishedDate = articleDto.PublishedDate
            };

            await _repository.UpdateArticle(article);
            return NoContent();
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> DeleteArticle(int id)
        {
            await _repository.DeleteArticle(id);
            return NoContent();
        }

        [HttpGet("paged")]
        public async Task<ActionResult<IEnumerable<Article>>> GetArticlesPaged([FromQuery] int page = 1, [FromQuery] int pageSize = 10)
        {
            var (articles, totalArticles) = await _repository.GetArticlesPaged(page, pageSize);

            Response.Headers.Add("X-Total-Count", totalArticles.ToString());
            return Ok(articles);
        }
    }
}
