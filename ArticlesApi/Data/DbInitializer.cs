﻿using ArticlesApi.Models;

namespace ArticlesApi.Data
{
    public static class DbInitializer
    {
        public static void Initialize(DataContext context)
        {
            context.Database.EnsureCreated();

            // Look for any users.
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }

            var users = new User[]
            {
                new User{ Username = "admin", Password = BCrypt.Net.BCrypt.HashPassword("admin"), Role = Role.Admin }
            };
            foreach (User u in users)
            {
                context.Users.Add(u);
            }

            context.SaveChanges();
        }
    }
}
