# ArticlesApi


# Articles API

This project is a basic REST API for managing articles using .NET 8. It includes functionalities to create, retrieve, update, and delete articles, with authentication implemented using JWT Bearer tokens and role-based authorization. The API also includes input validation using DTOs and FluentValidation.

## Assumptions and Design Decisions

- The API uses an in-memory database for simplicity and demonstration purposes.
- Authentication is implemented using JWT Bearer tokens.
- The repository pattern is used for data access to promote separation of concerns and testability.
- Role-based authorization is implemented to restrict certain actions to admin users only.
- Input validation is performed using DTOs and FluentValidation.

## How to Run

1. Clone the repository.
2. Navigate to the project directory.
3. Run `dotnet restore` to install dependencies.
4. Run `dotnet run` to start the application.
5. Use Postman or Swagger to interact with the API endpoints.  You can acces it from http://localhost:5182/swagger/index.html
6. For Swagger, after hitting the auth endpoint, you must use the Authorize button to set the fetched auth token for subsequent requests. The token must be entered into the Value field as: Bearer <token>

## Authentication

- Use the `/api/auth/login` endpoint to obtain a JWT token.
- Use as username admin and as password admin
- The authentification module is based on roles, for now there is one role: Admin that have a claim attached to it:  AdminOnly
- Include the token in the `Authorization` header as `Bearer {token}` for subsequent requests.

## Endpoints

### Authentication

#### Login

- **URL**: `/api/auth/login`
- **Method**: `POST`
- **Description**: Authenticates a user and returns a JWT token.
- **Request Body**:
  ```json
  {
    "username": "admin",
    "password": "admin"
  }


### Articles

#### Get All Articles

- **URL**: `/api/articles`
- **Method**: `GET`
- **Description**: Retrieves a list of all articles.
- **Request Body**:
  ```json
  [
  {
    "id": 1,
    "title": "Article 1",
    "content": "Content of article 1",
    "publishedDate": "2024-06-16T21:38:13.041Z"
  },
  {
    "id": 2,
    "title": "Article 2",
    "content": "Content of article 2",
    "publishedDate": "2024-06-16T21:38:13.041Z"
  }
  ]

#### Get Paginated Articles

- **URL**: `/api/articles/paged`
- **Method**: `GET`
- **Description**: Retrieves a paginated list of articles.
- **Authorization**: Not required.

- **Request Body**:
  ```json
  [
  {
    "id": 1,
    "title": "Article 1",
    "content": "Content of article 1",
    "publishedDate": "2024-06-16T21:38:13.041Z"
  },
  {
    "id": 2,
    "title": "Article 2",
    "content": "Content of article 2",
    "publishedDate": "2024-06-16T21:38:13.041Z"
  }
  ]


#### Get Articles By Id

- **URL**: `/api/articles/{id}`
- **Method**: `GET`
- **Description**: Retrieves a single article by its I.
- **Authorization**: Not required.

- **Request Body**:
  ```json
  {
  "id": 1,
  "title": "Article 1",
  "content": "Content of article 1",
  "publishedDate": "2024-06-16T21:38:13.041Z"
  }

#### Create an Article

- **URL**: `/api/articles`
- **Method**: `POST`
- **Description**: Creates a new article
- **Authorization**:  Admin only.

- **Request Body**:
  ```json
  {
    "title": "New Article",
    "content": "This is the content of the new article.",
    "publishedDate": "2024-06-16T21:38:13.041Z"
  }
- **Response**:
  ```json
  {
    "ID": 1
    "title": "New Article",
    "content": "This is the content of the new article.",
    "publishedDate": "2024-06-16T21:38:13.041Z"
  }

#### Update an Article

- **URL**: `/api/articles/{id}`
- **Method**: `PUT`
- **Description**: Updates an existing article by its ID
- **Authorization**:  Admin only.

- **Request Body**:
  ```json
  {
    "ID": 1,
    "title": "uPDATED  Article",
    "content": "This is the content of the updated article.",
    "publishedDate": "2024-06-16T21:38:13.041Z"
  }
- **Response**:
  ```json
  {
    "ID": 1,
    "title": "uPDATED  Article",
    "content": "This is the content of the updated article.",
    "publishedDate": "2024-06-16T21:38:13.041Z"
  }

#### Delete an Article

- **URL**: `/api/articles/{id}`
- **Method**: `DELETE`
- **Description**: Delete an  article by its ID
- **Authorization**:  Admin only.
- **Response**:  `204 No Content`









